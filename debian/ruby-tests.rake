require 'cucumber/rake/task'

Cucumber::Rake::Task.new(:features) do |t|
  t.bundler = false
end

task :default => :features
